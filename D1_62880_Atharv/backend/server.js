const express = require("express");

const app = express();

const cors = require("cors");

app.use(cors("*"));

app.use(express.json());

const routerMovie = require("./routes/employee");
app.use("/emp", routerMovie);

app.listen(4000, "0.0.0.0", () => {
    console.log("server started on port 4000");
});