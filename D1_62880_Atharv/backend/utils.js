function createResult(error, data) {
    const result = {};
    if(error){
        
        result.status =  'error';
        result.error = error;

    }else{
        result ["status"] = "success";
        result ["data"] = data;
    }

    return result;
}

const mysql = require("mysql2");

const pool = mysql.createPool({
    uri: "mysql://db:3306",
    user:'root',
    password:'root',
    database:'mydb',
    connectionLimit:20,
})

module.exports = {
    createResult,
    pool,
};