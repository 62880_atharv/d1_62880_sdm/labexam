const { query, response } = require("express");
const express = require("express");
const { request } = require("http");
const router = express.Router();
const utils = require("../utils");

//GET  --> Display Employee using name from Containerized MySQL
router.get("/get", (request, response) => {
    const { emp_Name } = request.body;

    const statement = `
        select * from Employee where
        emp_Name = ?
    `;
    utils.pool.query(statement,[emp_Name], (error, result) => {
        if(result.length > 0){
            console.log(result.length);
            console.log(result);
            response.send(utils.createResult(error, result));
        }else{
            response.send("Employee not found!!!");
        }
    });
});

//POST --> add employee details to the containerized MySQL
router.post("/add", (request, response) => {
    const { emp_Name,
        emp_Address,
        emp_Email,
        emp_MobileNo,
        emp_DOB,
        emp_JoiningDate } = request.body;

    const statement = `
        insert into Employee 
            (emp_Name,
                emp_Address,
                emp_Email,
                emp_MobileNo,
                emp_DOB,
                emp_JoiningDate)
        values
            (?, ?, ?, ?, ?, ?)    
    `;
    utils.pool.query(statement,[emp_Name,
        emp_Address,
        emp_Email,
        emp_MobileNo,
        emp_DOB,
        emp_JoiningDate], (error, result) => {
        response.send(utils.createResult(error, result));
    });
});

//UPDATE --> update employee details based on name to containerized MySQL
router.put("/update", (request, response) => {
    const { emp_Name, emp_Address, emp_Email, emp_MobileNo } = request.body;

    const statement = `
        update Employee 
        set
            emp_Address = ?,
            emp_Email = ?,
            emp_MobileNo = ?
        where
            emp_Name = ?
    `
    utils.pool.query(statement,[emp_Address, emp_Email, emp_MobileNo, emp_Name], (error, result) => {
        response.send(utils.createResult(error, result));
    });
});

//DELETE --> Delete employee from containerized MySQL 
router.delete("/remove", (request, response) => {
    const { emp_Name } = request.body;
    const statement = `
        delete from Employee 
            where
                emp_Name = ?
    `
    utils.pool.query(statement,[emp_Name], (error, result) => {
        response.send(utils.createResult(error, result));
    });
});


module.exports = router;
