CREATE table Employee(
    emp_id INTEGER primary key auto_increment,
    emp_Name VARCHAR(200),
    emp_Address VARCHAR(200),
    emp_Email VARCHAR(50),
    emp_MobileNo VARCHAR(20),
    emp_DOB date,
    emp_JoiningDate date
);